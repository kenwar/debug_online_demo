echo "==========创建nologin用户==============="
useradd debug_online_demo -s /sbin/nologin -g springboot
echo "==========进入git仓库==============="
cd /developer/git-repository/
echo "==========克隆项目==============="
git clone <GIT-URL>
echo "==========进入项目目录==============="
cd /developer/git-repository/debug_online_demo
echo "==========git切换分之到debug_online_demo-master==============="
git checkout master
echo "==================git fetch======================"
git fetch --all
echo "==================git reset --hard origin/master======================"
git reset --hard origin/master
echo "==================git pull======================"
git pull

echo "===========编译并跳过单元测试===================="
mvn clean package -Dmaven.test.skip=true

echo "============拷贝conf文件=============="
cp debug_online_demo.conf target/debug_online_demo.conf

echo "============建立软连接，以服务方式启动==================="
ln -s /developer/git-repository/debug_online_demo/target/debug_online_demo.jar /etc/init.d/debug_online_demo

echo "==========修改软链接所属人==============="
chown -R debug_online_demo:springboot /etc/init.d/debug_online_demo

echo "==========修改软链接权限==============="
chmod 777 /etc/init.d/debug_online_demo

echo "==========创建服务pid进程文件夹==============="
mkdir -p /var/run/debug_online_demo
echo "==========修改服务pid进程文件夹所属人==============="
chown -R debug_online_demo:springboot /var/run/debug_online_demo

echo "==========修改服务pid进程文件夹权限==============="
chmod 755 /var/run/debug_online_demo

echo "==========创建服务日志文件==============="
touch /var/log/debug_online_demo.log
echo "==========修改服务日志文件所属人==============="
chown -R debug_online_demo:springboot /var/log/debug_online_demo.log
echo "==========修改服务日志文件权限==============="
chmod 644 /var/log/debug_online_demo.log
echo "=========创建项目日志输出文件夹==============="
mkdir -p /opt/log/debug_online_demo
echo "=========修改项目日志输出文件夹所属人==============="
chown -R debug_online_demo:springboot /opt/log/debug_online_demo
echo "=========修改项目日志输出文件夹权限==============="
chmod 755 /opt/log/debug_online_demo
echo "============进入target==================="
cd target
echo "======修改jar权限与所属人======="
chown debug_online_demo:springboot debug_online_demo.*
chmod 500 debug_online_demo.*
echo "====================用nologin用户以服务方式启动====================="
sudo -u debug_online_demo service debug_online_demo start