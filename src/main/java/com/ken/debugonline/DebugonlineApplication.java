package com.ken.debugonline;

import com.alibaba.druid.pool.DruidDataSource;
import com.ken.debugonline.sender.MsgSender;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.ken.debugonline.dao")
public class DebugonlineApplication {

    public static void main(String[] args) {
        SpringApplication.run(DebugonlineApplication.class, args);
    }

    @Bean(initMethod = "init")
    public MsgSender msgSender(){
        return new MsgSender();
    }

    @Bean
    public DataSource dataSource(
            @Value("${dbs.database.username}") String userName,
            @Value("${dbs.database.password}") String password,
            @Value("${dbs.database.host}") String host,
            @Value("${dbs.database.port}") String port,
            @Value("${dbs.database.databasename}") String databasename
                                 ){
        DruidDataSource source = new DruidDataSource();
        source.setUrl("jdbc:mysql://"+host+":"+port+"/"+databasename+"?autoReconnect=true&amp;autoReconnectForPools=true&amp;useUnicode=true&amp;characterEncoding=utf-8&amp");
        source.setUsername(userName);
        source.setPassword(password);
        source.setMaxActive(2);
        source.setMaxWait(500000);
        return source;
    }


}
