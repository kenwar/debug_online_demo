package com.ken.debugonline.controller;

import com.ken.debugonline.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    private ITestService testService;

    @GetMapping("/send_msg")
    public String sendMsg(){
        testService.sendMsg();
        return "send msg ok";
    }

    @GetMapping("/query")
    public Integer query(){
        return testService.query();
    }
}
