package com.ken.debugonline.service.impl;

import com.ken.debugonline.dao.TestMapper;
import com.ken.debugonline.sender.MsgSender;
import com.ken.debugonline.service.ITestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TestServiceImpl implements ITestService {
    @Autowired
    private TestMapper testMapper;
    @Autowired MsgSender msgSender;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void sendMsg() {
        int id = testMapper.insert();
        testMapper.select(1L);
        // 模拟发送消息
        msgSender.sendMsg(id);

    }

    @Override
    public Integer query() {
        // 测试不在事务中的多个查询是否会重新获取连接
        testMapper.select(1L);
        testMapper.select(2L);
        testMapper.select(3L);
        return testMapper.select(4L);
    }
}
