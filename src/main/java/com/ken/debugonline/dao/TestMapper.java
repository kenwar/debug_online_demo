package com.ken.debugonline.dao;

public interface TestMapper {
    int insert();
    Integer select(Long id);
}
